class ApiService {
  token;
  constructor() {
    this.token = JSON.parse(sessionStorage.token);
  }

  async get(url, params) {
    const searchString = new URLSearchParams(params ?? {}).toString();
    const _url = `${url}?${searchString}`;
    const res = await fetch(_url, {
      headers: {
        Cdtoken: this.token
      }
    });
    return res.json();
  }

  async post(url, data) {
    const res = await fetch(url, {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Cdtoken: this.token
      }
    });
    return res.json();
  }

}

const apiService = new ApiService();

const matterList = [
  {
    "id": "14",
    "matterName": "西区网球场"
  },
  {
    "id": "20",
    "matterName": "风雨操场羽毛球场",
  },
  {
    "id": "21",
    "matterName": "乒乓球",
  },
  {
    "id": "23",
    "matterName": "东区网球场",
  },
  {
    "id": "24",
    "matterName": "体育馆二楼羽毛球馆",
  }
];

async function getMatterList(date, matterSmallTypeId) {
  const url = '/api/api/matter/matterInfo/getMatterList';
  const params = {
    appolitPeriodDay: date,
    campusCode: 13,
    matterBigTypeId: 3,
    matterName: '',
    matterSmallTypeId: matterSmallTypeId,
    matterClassId: 1000
  };
  const res = await apiService.get(url, params);
  return res.rows;
}

async function getMatterNumList(appolitPeriodDay, id) {
  const url = '/api/api/matter/matterInfo/getMatterNumList';
  const data = {
    appolitPeriodDay,
    id,
  };
  const res = await apiService.post(url, data);
  return res.data;
}

async function submit({
  name,
  phone,
  appolitPeriodDay,
  appolitPeriodTime,
  matterInfoId,
  matterClassName,
}) {
  const url = '/api/api/order/add';
  const data = {
    "frontNotifyUrl": "http://cgzx.scu.edu.cn/#/paySuccess?inout=0&id=3&orderId=",
    "appolitPeriodDay": appolitPeriodDay,
    "appolitPeriodTime": appolitPeriodTime,
    "matterInfoId": matterInfoId,
    "memberName": name,
    "memberPhone": phone,
    "payPriType": 1,
    "matterClassName": matterClassName,
  };
  const res = await apiService.post(url, data);
  return res;
}

async function start({name, phone, date, time, matterSmallTypeId, chooseArr ,interval}) {
  const timer = setInterval(async() => {
    const list = await getMatterList(date, matterSmallTypeId);

    let choosedId = null;

    for (const choosedMatterName of chooseArr) {
      const matterItem = list.find(v => v.matterName === choosedMatterName);
      if (matterItem?.status === 2) {
        const { id } = matterItem;
        const timeList = await getMatterNumList(date, id);
        let find = false;
        for (const timeItem of timeList) {
          if (timeItem.servIntervalTime === time && timeItem.sureMatterNum === 1) {
            find = true;
            break;
          }
        }
        if (find) {
          choosedId = id;
          break;
        }
      }
    }
    if (choosedId === null) {
      console.log('暂未开始或已经抢完,尝试再次预约');
    } else {
      const matterClassName = matterList.find(v => v.id === matterSmallTypeId).matterName;
      const { code } = await submit({
        name,
        phone,
        appolitPeriodDay: date,
        appolitPeriodTime: time,
        matterInfoId: choosedId,
        matterClassName,
      });

      if (code === 200) {
        console.log('预约成功，已自动退出');
        clearInterval(timer);
      } else {
        console.log('预约失败尝试再次预约');
      }
    }
  }, interval);
}

;(async() => {
  const config = {
    name: '祝煌昆',
    phone: '13018232621',
    date: '2024-04-25',
    time: '12:00-13:00',
    matterSmallTypeId: '24',
    chooseArr: ['03', '04', '07', '08'],
    interval: 500,
  };
  await start(config);
})();
